// funcion anonima
let j = function (Hola){
    
    alert("Hola mundo");
    return true;
    
}

// funcion flecha
let y = Hola => alert("funcion flecha");

// funcion callback
function showAlert(){
    alert('Alerta');
 }
 button.addEventListener('click', showAlert);